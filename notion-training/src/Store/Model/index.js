import { todoListModel, todoListSelector } from './TodoList';

const storeModel = {
    todoList: todoListModel,
};

export { storeModel as default, todoListSelector };
