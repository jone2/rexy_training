const todoList = {
    refreshTodoList: (actions) => actions.todoList.moveTodo,
    getTodoList: (state) => state.todoList.data,
    // Handle Todo
    addTodoAction: (actions) => actions.todoList.addTodo,
    updateTodoAction: (actions) => actions.todoList.updateTodo,
    removeTodoAction: (actions) => actions.todoList.removeTodo,
    // Follow code example
    loadDataAction: (actions) => actions.todoList.loadData,
    changeParamsAction: (actions) => actions.todoList.changeParams,
    firstLoadState: (state) => state.todoList.firstLoad,
    loadingState: (state) => state.todoList.loading,
    todoListState: (state) => state.todoList,
};

export default todoList;
