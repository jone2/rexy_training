// Thu vien dung chung
import { action, computed, thunkOn } from 'easy-peasy';
import { v4 as uuidv4 } from 'uuid';
import _cloneDeep from 'lodash/cloneDeep';
// Nhung thu khac
import { getTodos } from '../../../api/todoAPI';

const todoListModel = {
    firstLoad: true,
    loading: true,
    data: [],
    params: {
        total_count: 0,
        keyword: '',
    },

    todosComputed: computed((state) => {
        const todoList = state.data;
        const notStarted = todoList.filter((todo) => todo.status === 'NOT STARTED');
        const inProgress = todoList.filter((todo) => todo.status === 'IN PROGRESS');
        const done = todoList.filter((todo) => todo.status === 'DONE');
        return [notStarted, inProgress, done];
    }),
    addTodo: action((state, todo) => {
        todo.id = uuidv4();
        state.data = [...state.data, todo];
    }),
    removeTodo: action((state, id) => {
        const todoList = state.data;
        state.data = todoList.filter((todo) => todo.id !== id);
    }),
    updateTodo: action((state, todoUpdate) => {
        const updateItemIndex = state.data.findIndex((item) => item.id === todoUpdate.id);
        const newData = _cloneDeep(state.data);
        newData.splice(updateItemIndex, 1, todoUpdate);
        state.data = [...newData];
    }),

    // Update position of todo in column
    moveTodo: action((state, payload) => {
        state.data = [...payload];
    }),

    // Follow code example
    setData: action((state, { data, total_count }) => {
        state.params.total_count = total_count;
        state.data = data;
        state.loading = false;
        state.firstLoad = false;
    }),

    changeParams: action((state, { params }) => {
        state.params = { ...state.params, ...params };
    }),

    setLoading: action((state, loading) => {
        state.loading = loading;
    }),

    loadData: action((state, payload = {}) => {
        const params = payload.params || {};
        state.params = { ...state.params, ...params };
        state.loading = true;
    }),

    loadDataListener: thunkOn(
        (actions) => [actions.changeParams, actions.loadData],
        async (actions, target, { getState, getStoreStore }) => {
            const { loading, params } = getState();
            if (!loading) {
                actions.setLoading(true);
            }
            try {
                const res = await getTodos(params);
                actions.setData(res);
            } catch (e) {
                console.log(e);
                actions.setLoading(true);
            }
        }
    ),
};

export default todoListModel;
