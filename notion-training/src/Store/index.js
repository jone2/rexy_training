import { createStore } from 'easy-peasy';
import models from './Model';

const store = createStore(models);

export default store;
