export const ROUTERS = {
    Home: '/',
    Todos: '/todos',
    AddTodo: '/todos/add-todo',
    UpdateTodo: '/todos/:id',
    Login: '/login',
};
