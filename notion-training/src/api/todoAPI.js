import { data } from '../Data/data';
import debounce from 'lodash/debounce';

// const handleResult = (result = {}) => {
//     return new Promise((resolve) => {
//         debounce(() => {
//             resolve(result);
//         })();
//     });
// };

const getTodos = async (params) => {
    const totalData = data.todos;
    if (params.keyword) {
        const arr = totalData.filter((item) => item.title.includes(params.keyword));
        return new Promise((resolve) => {
            debounce(() => {
                resolve({
                    data: arr,
                    total_count: arr.length,
                });
            }, 2000)();
        });
    }
    return new Promise((resolve) => {
        debounce(() => {
            resolve({
                data: totalData,
                total_count: 2,
            });
        }, 2000)();
    });
};

export { getTodos };
