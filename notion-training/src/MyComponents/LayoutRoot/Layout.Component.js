import React, { useState } from 'react';
import { NavLink } from 'react-router-dom';

import {
    Layout,
    Menu,
    MenuFoldOutlined,
    MenuUnfoldOutlined,
    Typography,
    UserOutlined,
    VideoCameraOutlined,
} from '../../LibComponents';

import styles from './styles.module.scss';

const { Header, Sider, Content } = Layout;

const LayoutRoot = ({ children }) => {
    const [collapsed, setCollapsed] = useState(false);
    const toggle = () => {
        setCollapsed(!collapsed);
    };
    return (
        <Layout>
            <Sider trigger={null} collapsible collapsed={collapsed}>
                <div className={styles.logo}>
                    <Typography className={styles.color_logo}>REXY TRAINING</Typography>
                </div>
                <Menu theme="dark" mode="inline" defaultSelectedKeys={['1']}>
                    <Menu.Item key="1" icon={<UserOutlined />}>
                        <NavLink to="/">Home</NavLink>
                    </Menu.Item>
                    <Menu.Item key="2" icon={<VideoCameraOutlined />}>
                        <NavLink to="/todos">Todos</NavLink>
                    </Menu.Item>
                </Menu>
            </Sider>
            <Layout className={styles.site_layout}>
                <Header className={styles.site_layout_background}>
                    {React.createElement(
                        collapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
                        {
                            className: `${styles.trigger}`,
                            onClick: toggle,
                        }
                    )}
                </Header>
                <Content
                    className={
                        (styles.site_layout_background,
                        styles.site_layout_background_extend)
                    }
                >
                    {children}
                </Content>
            </Layout>
        </Layout>
    );
};

export default LayoutRoot;
