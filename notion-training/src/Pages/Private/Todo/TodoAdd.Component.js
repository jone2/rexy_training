//Thu vien ngoai
import React from 'react';
import { labels, status } from '../../../Data/data';
import { useStoreActions } from 'easy-peasy';
import { useHistory } from 'react-router-dom';
import _cloneDeep from 'lodash/cloneDeep';
// Cac thu muc dung chung
import {
    Button,
    Col,
    Form,
    Input,
    Modal,
    Row,
    Select,
    Typography,
} from '../../../LibComponents';
import { todoListSelector } from '../../../Store/Model';
// Cac components cung thu muc

// Nhung thanh phan khac
const { Option } = Select;

const TodoAdd = () => {
    const history = useHistory();
    const [form] = Form.useForm();
    const addTodo = useStoreActions(todoListSelector.addTodoAction);
    const onFinish = (values) => {
        const oldData = _cloneDeep(values);
        const newData = { ...oldData, label: changeLabels(oldData.label) };
        addTodo(newData);
        form.resetFields();
        closeModal();
    };

    const changeLabels = (listLabel) => labels.filter((item) => listLabel.includes(item.text));

    const onFinishFailed = () => {
        console.log('onFinishFailed');
    };

    const closeModal = () => {
        history.push('/todos');
    };

    return (
        <Modal
            visible={true}
            centered
            footer={null}
            maskClosable={false}
            closable={false}
            width={'70%'}
            bodyStyle={{ overflow: 'auto' }}
        >
            <Form
                name="detailTodo"
                form={form}
                onFinish={onFinish}
                onFinishFailed={onFinishFailed}
            >
                <Form.Item name="title">
                    <Input type="text" />
                </Form.Item>
                <Row>
                    <Col span={8}>
                        <div className="h-14">
                            <Typography>Assign</Typography>
                        </div>
                        <div className="h-14">
                            <Typography>Label</Typography>
                        </div>
                        <div className="h-14">
                            <Typography>Status</Typography>
                        </div>
                        <div className="h-14">
                            <Typography>Description</Typography>
                        </div>
                    </Col>
                    <Col span={16}>
                        <div>
                            <Form.Item name="responsiblePerson">
                                <Input />
                            </Form.Item>
                        </div>
                        <div>
                            <Form.Item name="label">
                                <Select
                                    className="w-full"
                                    mode="tags"
                                    size="middle"
                                    placeholder="Please select"
                                >
                                    {labels.map((label, index) => (
                                        <Option key={index} value={label.text}>
                                            {label.text}
                                        </Option>
                                    ))}
                                </Select>
                            </Form.Item>
                        </div>
                        <div>
                            <Form.Item name="status">
                                <Select
                                    size="middle"
                                    placeholder="Please select"
                                    className="w-full"
                                >
                                    {status.map((element, index) => (
                                        <Option key={index} value={element.text}>
                                            {element.text}
                                        </Option>
                                    ))}
                                </Select>
                            </Form.Item>
                        </div>
                        <div>
                            <Form.Item name="description">
                                <Input.TextArea />
                            </Form.Item>
                        </div>
                    </Col>
                </Row>
                <div className="flex justify-end">
                    <Button type="default" htmlType="button" onClick={closeModal}>
                        Cancel
                    </Button>
                    <Form.Item className="ml-4">
                        <Button type="primary" htmlType="submit">
                            Save
                        </Button>
                    </Form.Item>
                </div>
            </Form>
        </Modal>
    );
};

export default TodoAdd;
