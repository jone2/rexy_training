import React from 'react';

import { Row, Col, Typography } from '../../../LibComponents';

import styles from './TodoListHeader.module.scss';

function TodoListHeader({ status }) {
    return (
        <Row>
            {status.map((element, index) => (
                <Col span={8} key={index}>
                    <div className={styles.status}>
                        <Typography style={{ color: `${element.color}` }}>
                            {element.text}
                        </Typography>
                    </div>
                </Col>
            ))}
        </Row>
    );
}

export default TodoListHeader;
