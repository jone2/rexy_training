//Thu vien ngoai
import React from 'react';
import { useStoreActions, useStoreState } from 'easy-peasy';
import { DragDropContext } from 'react-beautiful-dnd';
import _cloneDeep from 'lodash/cloneDeep';
import _flattenDeep from 'lodash/flattenDeep';
// Cac thu muc dung chung
import { Row } from '../../../LibComponents';
import { todoListSelector } from '../../../Store/Model';
// Cac components cung thu muc
import TodoColumn from './TodoColumn.Component';
import TodoListLoading from './TodoListLoading.Component';

function TodoList(props) {
    const moveTodoInColumn = useStoreActions(todoListSelector.refreshTodoList);

    const reorder = (list, startIndex, endIndex) => {
        const result = Array.from(list);
        const [removed] = result.splice(startIndex, 1);
        result.splice(endIndex, 0, removed);
        return result;
    };

    const move = (source, destination, droppableSource, droppableDestination) => {
        const sourceClone = Array.from(source);
        const destClone = Array.from(destination);
        const [removed] = sourceClone.splice(droppableSource.index, 1);
        removed.status = droppableDestination.droppableId;
        destClone.splice(droppableDestination.index, 0, removed);
        const sourceIndex = getIndexList(droppableSource.droppableId);
        const destIndex = getIndexList(droppableDestination.droppableId);
        const result = {
            sourceIndex: sourceIndex,
            destIndex: destIndex,
            sourceClone: sourceClone,
            destClone: destClone,
        };
        return result;
    };

    const getList = (status) => {
        return status === 'NOT STARTED'
            ? todosComputed[0]
            : status === 'IN PROGRESS'
            ? todosComputed[1]
            : todosComputed[2];
    };

    const getIndexList = (status) => {
        return status === 'NOT STARTED' ? 0 : status === 'IN PROGRESS' ? 1 : 2;
    };

    const changeTodos = (params) => {
        const newTodos = _cloneDeep(todosComputed);
        newTodos.splice(params.sourceIndex, 1, params.sourceClone);
        newTodos.splice(params.destIndex, 1, params.destClone);
        return newTodos;
    };

    const handleOnDragEnd = (result) => {
        const { source, destination } = result;
        if (!destination) return;
        if (source.droppableId === destination.droppableId) {
            const items = reorder(
                getList(source.droppableId),
                source.index,
                destination.index
            );
            const newTodos = _cloneDeep(todosComputed);
            newTodos[getIndexList(source.droppableId)] = items;
            moveTodoInColumn(_flattenDeep(newTodos));
        } else {
            const result = move(
                getList(source.droppableId),
                getList(destination.droppableId),
                source,
                destination
            );
            const newTodos = changeTodos(result);
            moveTodoInColumn(_flattenDeep(newTodos));
        }
    };

    const {
        loading,
        todosComputed,
        params: { total_count },
    } = useStoreState(todoListSelector.todoListState);

    // const todoList = useStoreState(todoListSelector.todoListState);
    // console.log(todoList);

    if (!loading && total_count < 1) {
        return <div>Empty</div>;
    }

    return (
        <div>
            {loading ? (
                <TodoListLoading />
            ) : (
                <DragDropContext onDragEnd={handleOnDragEnd}>
                    <Row gutter={[16, 16]}>
                        {todosComputed.map((data, index) => {
                            const name =
                                index === 0
                                    ? 'NOT STARTED'
                                    : index === 1
                                    ? 'IN PROGRESS'
                                    : 'DONE';
                            return <TodoColumn key={index} data={data} status={name} />;
                        })}
                    </Row>
                </DragDropContext>
            )}
        </div>
    );
}

export default TodoList;
