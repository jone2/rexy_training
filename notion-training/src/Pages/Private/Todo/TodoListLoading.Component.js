import React from 'react';

function TodoListLoading(props) {
    return <div>Loading ...</div>;
}

export default TodoListLoading;
