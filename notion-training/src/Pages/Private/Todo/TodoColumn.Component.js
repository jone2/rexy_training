//Thu vien ngoai
import React from 'react';
import { Droppable } from 'react-beautiful-dnd';

// Cac thu muc dung chung
import { Col } from '../../../LibComponents';

// Cac components cung thu muc
import TodoListItem from './TodoListItem.Component';

const TodoColumn = ({ status, data = [] }) => {
    return (
        <Col span={8}>
            <Droppable droppableId={status}>
                {(provided) => (
                    <ul
                        {...provided.droppableProps}
                        ref={provided.innerRef}
                        className="min-h-full"
                    >
                        {data.map((todo, index) => (
                            <TodoListItem key={todo.id} todo={todo} index={index} />
                        ))}
                        {provided.placeholder}
                    </ul>
                )}
            </Droppable>
        </Col>
    );
};

export default TodoColumn;
