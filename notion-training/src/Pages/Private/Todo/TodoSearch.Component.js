//Thu vien ngoai
import React, { useState } from 'react';
import { useStoreActions, useStoreState } from 'easy-peasy';
import debounce from 'lodash/debounce';
// Cac thu muc dung chung
import { Input } from '../../../LibComponents';
import { todoListSelector } from '../../../Store/Model';
// Cac components cung thu muc

function TodoSearch(props) {
    const [localState, setLocalState] = useState({ keyword: '' });
    const loading = useStoreState(todoListSelector.loadingState);
    const changeParams = useStoreActions(todoListSelector.changeParamsAction);

    const handleChange = (event) => {
        setLocalState((prev) => ({ ...prev, keyword: event.target.value }));

        const handleChangeParams = debounce(() => {
            changeParams({ params: { keyword: event.target.value } });
        }, 500);
        handleChangeParams();
    };

    return (
        <Input
            className="max-w-sm"
            type="text"
            value={localState.keyword}
            disabled={loading}
            placeholder="Search to do"
            onChange={handleChange}
        />
    );
}

export default TodoSearch;
