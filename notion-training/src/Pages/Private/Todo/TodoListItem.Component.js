//Thu vien ngoai
import React from 'react';
import { useStoreActions } from 'easy-peasy';
import { useHistory } from 'react-router-dom';
import { Draggable } from 'react-beautiful-dnd';
// Cac thu muc dung chung
import { Avatar, Card, Tag, Typography, UserOutlined } from '../../../LibComponents';
import { todoListSelector } from '../../../Store/Model';
// Cac components cung thu muc
import styles from './TodoListItem.module.scss';

const TodoListItem = ({ todo, index }) => {
    const history = useHistory();
    const tagTodo = todo.label;
    const removeTodo = useStoreActions(todoListSelector.removeTodoAction);

    const removeTodoClick = (e) => {
        e.stopPropagation();
        removeTodo(todo.id);
    };

    const showModal = () => {
        history.push(`/todos/${todo.id}`);
    };
    return (
        <Draggable key={todo.id} draggableId={todo.id} index={index}>
            {(provided) => (
                <li
                    {...provided.draggableProps}
                    {...provided.dragHandleProps}
                    ref={provided.innerRef}
                >
                    <Card
                        title={todo.title}
                        extra={
                            <Typography
                                className={styles.removeBtn}
                                onClick={removeTodoClick}
                            >
                                Remove
                            </Typography>
                        }
                        onClick={showModal}
                        className="w-full mb-4 shadow-md"
                        bodyStyle={{ padding: '0.5rem' }}
                    >
                        <div className="flex items-center">
                            <Avatar size="small" icon={<UserOutlined />} />
                            <Typography className="py-2 ml-2">
                                {todo.responsiblePerson}
                            </Typography>
                        </div>
                        <div>
                            {tagTodo.map((tag, index) => (
                                <Tag key={index} className="mb-2" color={tag.color}>
                                    {tag.text}
                                </Tag>
                            ))}
                        </div>
                    </Card>
                </li>
            )}
        </Draggable>
    );
};

export default TodoListItem;
