import { useEffect } from 'react';
import { useStoreActions, useStoreState } from 'easy-peasy';

import { todoListSelector } from '../../../Store/Model';

const useTodoHook = () => {
    const loadData = useStoreActions(todoListSelector.loadDataAction);
    const firstLoad = useStoreState(todoListSelector.firstLoadState);

    useEffect(() => {
        loadData();
    }, [loadData]);

    return { firstLoad };
};

export default useTodoHook;
