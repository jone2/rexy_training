// Thu vien ngoai
import React from 'react';
import { status } from '../../../Data/data';
import { Switch, Route, useHistory } from 'react-router-dom';
// Cac thu muc dung chung
import { Loading } from '../../../MyComponents';
import { Button } from '../../../LibComponents';
import useTodoHook from './Todo.Hook';
// Cac component cung thu muc
import TodoList from './TodoList.Component';
import TodoListHeader from './TodoListHeader.Component';
import TodoSearch from './TodoSearch.Component';
import TodoAdd from './TodoAdd.Component';
import TodoUpdate from './TodoUpdate.Component';

function Todo(props) {
    const { firstLoad } = useTodoHook();
    const history = useHistory();
    return (
        <div>
            {firstLoad ? (
                <Loading />
            ) : (
                <div>
                    <div className="flex justify-between">
                        <TodoSearch />
                        <Button
                            onClick={() => {
                                history.push('/todos/add-todo');
                            }}
                        >
                            Add todo
                        </Button>
                    </div>
                    <TodoListHeader status={status} />
                    <TodoList />
                    <Switch>
                        <Route exact path="/todos/add-todo" component={TodoAdd} />
                        <Route exact path="/todos/:id" component={TodoUpdate} />
                    </Switch>
                </div>
            )}
        </div>
    );
}

export default Todo;
