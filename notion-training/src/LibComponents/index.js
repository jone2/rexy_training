export {
    Button,
    Progress,
    Layout,
    Menu,
    Row,
    Col,
    Card,
    Typography,
    Tag,
    Avatar,
    Modal,
    Form,
    Input,
    Select,
    Skeleton,
} from 'antd';

export {
    MenuUnfoldOutlined,
    MenuFoldOutlined,
    UserOutlined,
    VideoCameraOutlined,
    UploadOutlined,
    FileTextOutlined,
} from '@ant-design/icons';
