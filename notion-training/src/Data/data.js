const status = [
    {
        text: 'NOT STARTED',
        color: 'red',
    },
    {
        text: 'IN PROGRESS',
        color: 'orange',
    },
    {
        text: 'DONE',
        color: 'blue',
    },
];
const labels = [
    {
        text: 'UI',
        color: 'red',
    },
    {
        text: 'ADMIN',
        color: 'blue',
    },
    {
        text: 'EXTRA',
        color: 'cyan',
    },
    {
        text: 'WEB',
        color: 'green',
    },
    {
        text: 'RESEARCH',
        color: 'purple',
    },
];

const data = {
    todos: [
        {
            id: '1',
            title: 'Research dragdrop',
            label: [labels[0], labels[1]],
            status: status[0].text,
            responsiblePerson: 'Jone Nguyen',
            description:
                'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using , making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for  will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).',
        },
        {
            id: '2',
            title: 'Add, Remove, Update todo',
            label: [labels[2], labels[3]],
            status: status[1].text,
            responsiblePerson: 'Jone Nguyen',
            description:
                'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using , making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for  will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).',
        },
        {
            id: '3',
            title: 'Create todo list app',
            label: [labels[3], labels[4], labels[1]],
            status: status[2].text,
            responsiblePerson: 'Jone Nguyen',
            description:
                'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using , making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for  will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).',
        },
        {
            id: '4',
            title: 'Add drag drop for todo',
            label: [labels[0], labels[1], labels[3]],
            status: status[0].text,
            responsiblePerson: 'Jone Nguyen',
            description:
                'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using , making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for  will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).',
        },
        {
            id: '5',
            title: 'Set up the initial project',
            label: [labels[1], labels[3], labels[4]],
            status: status[0].text,
            responsiblePerson: 'Jone Nguyen',
            description:
                'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using , making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for  will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).',
        },
        {
            id: '6',
            title: 'test',
            label: [labels[1], labels[3], labels[4]],
            status: status[1].text,
            responsiblePerson: 'Jone Nguyen',
            description:
                'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using , making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for  will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).',
        },
    ],
};

export { data, labels, status };
