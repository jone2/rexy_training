// Thu vien ngoai
import React from 'react';
import { Redirect, Route } from 'react-router-dom';
// Thu muc dung chung
import { LayoutRoot } from '../MyComponents';
import { ROUTERS } from '../Constants';

// Thu muc khac

function PrivateRoute({ children, ...rest }) {
    console.log(rest);
    let auth = { user: true };
    return (
        <Route
            {...rest}
            render={({ location }) =>
                auth.user ? (
                    <LayoutRoot>{children}</LayoutRoot>
                ) : (
                    <Redirect
                        to={{ pathname: `${ROUTERS.Login}`, state: { from: location } }}
                    />
                )
            }
        />
    );
}

export default PrivateRoute;
