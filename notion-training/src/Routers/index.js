import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { ROUTERS } from '../Constants';
import { Login, Home } from '../Pages';
import Todo from '../Pages/Private/Todo';
import PrivateRoute from './AuthRoute';

const Root = () => {
    return (
        <Router>
            <Switch>
                <PrivateRoute exact path={ROUTERS.Home}>
                    <Home />
                </PrivateRoute>
                <PrivateRoute path={ROUTERS.Todos}>
                    <Todo />
                </PrivateRoute>
                <Route path={ROUTERS.Login}>
                    <Login />
                </Route>
            </Switch>
        </Router>
    );
};

export default Root;
